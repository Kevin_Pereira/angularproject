export const environment = {
  production: true,
  application:
    {
      name: 'Keveen',
      angular: 'Angular 8.2.14',
      bootstrap: 'Bootstrap 4.3.1',
    }
};
